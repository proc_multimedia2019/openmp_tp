
all : HelloWorld InfoMachine

HelloWorld : HelloWorld.c
	gcc -fopenmp HelloWorld2.c -o HelloWorld

InfoMachine : InfoMachine.c
	gcc -fopenmp InfoMachine.c -o InfoMachine
clean :
	rm HelloWorld InfoMachine


